from django.contrib import admin
from rating.models import NotRegisteredUser, Ratee, Users, Rate, Comments

admin.site.register(NotRegisteredUser)
admin.site.register(Ratee)
admin.site.register(Users)
admin.site.register(Comments)
admin.site.register(Rate)