from .client import get_photo
from .bot_start import start, register, rate, rate_id, comment, choose_func, choice, BotState, invalid, command_invalid
from .deep_link import BotStateLink, register_link, comment_link, func_link, rate_link