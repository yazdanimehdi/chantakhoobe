import os
from enum import IntEnum

import django
import telegram
from .client import get_photo

class BotStateLink(IntEnum):
    MODE = 7
    REGISTER = 8
    RATE = 9
    COMMENT = 10


def func_link(bot, update, user_data):
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'reserve_site.settings')
    django.setup()
    from rating.models import Users, Ratee
    registered_db = Users.objects.filter(chat_id=user_data['chat_id'])
    if registered_db:
        user_data['user'] = registered_db[0]
        if update.message.text == 'بگم چنتا🤪':
            user_data['mode'] = 1
            keyboard = list()
            for i in range(1, 11):
                keyboard.append([telegram.InlineKeyboardButton(f'{i}', callback_data=f'{i}')])
            reply_markup = telegram.InlineKeyboardMarkup(keyboard)
            bot.sendMessage(chat_id=update.message.chat_id,
                            text="*بگو چنتا خوبه؟*",
                            reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
            return BotStateLink.RATE
        elif update.message.text == 'نظر بنویسم😈':
            user_data['mode'] = 2
            bot.sendMessage(chat_id=update.message.chat_id,
                            text="*نظرتو راجع بهش به صورت ناشناس بگو*",
                            parse_mode=telegram.ParseMode.MARKDOWN)
            return BotStateLink.COMMENT
    else:
        reply_markup = telegram.ReplyKeyboardMarkup(
            [[telegram.KeyboardButton('ثبت نام', request_contact=True)]], one_time_keyboard=True)
        bot.sendMessage(chat_id=update.message.chat_id,
                        text="*برای این که بتونی بگی چنتا خوبه دکمه‌ی زیر رو لمس کن*",
                        reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
        return BotStateLink.REGISTER


def rate_link(bot, update, user_data):
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'reserve_site.settings')
    django.setup()
    from rating.models import Ratee, Rate, Users
    query = update.callback_query
    ratee = Ratee.objects.get(id=user_data['ratee'])
    rate = Rate.objects.filter(ratee_user=ratee, rating_user=user_data['user'])
    if rate:
        bot.sendMessage(chat_id=user_data['chat_id'],
                        text="*یه بار گفتی دیگه😒*",
                        parse_mode=telegram.ParseMode.MARKDOWN)
    else:
        rate = Rate()
        rate.ratee_user = ratee
        rate.rating_user = user_data['user']
        rate.rate = int(query['data'])
        rate.save()

        a = Rate.objects.filter(ratee_user=ratee)
        total = 0
        for i in a:
            total += i.rate
        average = total/a.count()
        bot.sendMessage(chat_id=user_data['chat_id'],
                        text="*اکی شد\n"
                             f"تا الان این دوستمون \n"
                             f"{average}\n"
                             f"تا خوبه!!!*",
                        parse_mode=telegram.ParseMode.MARKDOWN)

        bot.sendMessage(chat_id=ratee.ratee_user.user_id,
                        text=f"*یه نفر بهت گفته  \n"
                             f"{rate.rate}\n"
                             f"تا خوبی!!!*",
                        parse_mode=telegram.ParseMode.MARKDOWN)

    keyboard = [[telegram.KeyboardButton('چندتا خوبم؟')], [telegram.KeyboardButton('چندتا خوبه؟')],
                [telegram.KeyboardButton('نظرها به من')], [telegram.KeyboardButton('لینکمو بساز')]]
    reply_markup = telegram.ReplyKeyboardMarkup(keyboard, one_time_keyboard=True)
    bot.sendMessage(chat_id=user_data['chat_id'],
                    text="*دیگه چیکار می‌تونم برات بکنم؟*",
                    reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
    return 1


def comment_link(bot, update, user_data):
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'reserve_site.settings')
    django.setup()
    from rating.models import Comments, Ratee
    ratee = Ratee.objects.get(id=user_data['ratee'])
    comment = Comments()
    comment.ratee_user = ratee
    comment.rating_user = user_data['user']
    comment.comments = update.message.text
    comment.save()

    bot.sendMessage(chat_id=user_data['chat_id'],
                    text="نظرت ثبت شد",
                    parse_mode=telegram.ParseMode.MARKDOWN)
    bot.sendMessage(chat_id=ratee.ratee_user.user_id,
                    text=f"*یه نفر بهت نظر جدید داده و گفته که: \n"
                         f"{comment.comments}*",
                    parse_mode=telegram.ParseMode.MARKDOWN)

    keyboard = [[telegram.KeyboardButton('چندتا خوبم؟')], [telegram.KeyboardButton('چندتا خوبه؟')],
                [telegram.KeyboardButton('نظرها به من')], [telegram.KeyboardButton('لینکمو بساز')]]
    reply_markup = telegram.ReplyKeyboardMarkup(keyboard, one_time_keyboard=True)
    bot.sendMessage(chat_id=user_data['chat_id'],
                    text="*دیگه چیکار می‌تونم برات بکنم؟*",
                    reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
    return 1


def register_link(bot, update, user_data):
    if update.message.contact.phone_number[0] == '+':
        phone = update.message.contact.phone_number
    else:
        phone = '+' + update.message.contact.phone_number

    user_id = update.message.contact.user_id

    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'reserve_site.settings')
    django.setup()
    from rating.models import Users, NotRegisteredUser, Ratee
    u = NotRegisteredUser.objects.filter(user_id=user_id)
    if u:
        user = Users()
        user.user_id = u[0].user_id
        user.username = u[0].user_name
        user.phone = phone
        user.chat_id = update.message.chat_id
        user.save()
        ratee = Ratee.objects.filter(notuser__user_id=u[0].user_id)
        if ratee:
            ratee.ratee_user = user
            ratee.save()
        u.delete()
    else:
        user = Users()
        user.user_id = user_id
        user.phone = phone
        user.chat_id = update.message.chat_id
        user.save()

    keyboard = [[telegram.KeyboardButton('بگم چنتا🤪')], [telegram.KeyboardButton('نظر بنویسم😈')]]
    reply_markup = telegram.ReplyKeyboardMarkup(keyboard, one_time_keyboard=True)
    bot.sendMessage(chat_id=update.message.chat_id,
                    text="*می‌خوای چیکار کنی؟ 😬*",
                    reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
    return BotStateLink.MODE