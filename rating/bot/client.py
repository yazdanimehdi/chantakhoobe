from pyrogram import Client, InputPhoneContact
import shutil
import os
from os import listdir
from os.path import isfile, join
from django.core.files import File


def get_photo(user_model, user_id):
    app = Client("my_account",
                 api_id=771644,
                 api_hash="ddc9a893a788fddeffd15d4717dc2405"
                 )
    with app:
        try:
            app.download_media(app.get_user_profile_photos(user_id)['photos'][0])
            if os.path.isdir("./downloads"):
                mypath = './downloads'
                onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
                f = open(f'./downloads/{onlyfiles[0]}', 'rb')
                user_model.image = File(f)
                user_model.save()
                f.close()
                shutil.rmtree('./downloads')
            return True
        except:
            return False


def get_user_id(name, username=None, phone=None):
    app = Client("my_account",
                 api_id=771644,
                 api_hash="ddc9a893a788fddeffd15d4717dc2405"
                 )
    if username is not None:
        try:
            with app:
                return app.get_users(user_ids=username)['id'], True
        except Exception as e:
            print(e)
            return None, False
    elif phone is not None:
        with app:
            try:
                contacts = app.get_contacts()['users']
                lis = list()
                for contact in contacts:
                    lis.append(contact.id)
                app.delete_contacts(lis)
                return app.add_contacts([InputPhoneContact(phone=phone, first_name=name)])['imported'][0].user_id, True
            except:
                return None, False



