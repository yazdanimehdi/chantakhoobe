import os
from enum import IntEnum

import django
import telegram
from telegram.ext import ConversationHandler
from .client import get_photo, get_user_id
from django.db.models import Q
from .deep_link import BotStateLink


class BotState(IntEnum):
    CHOICE = 1
    REGISTER = 2
    Rate_ID = 3
    CHOOSEFUNC = 4
    COMMENT = 5
    RATE = 6


def start(bot, update, user_data, args):
    if args == []:
        user_data['chat_id'] = update.message.chat_id
        keyboard = [[telegram.KeyboardButton('چندتا خوبم؟')], [telegram.KeyboardButton('چندتا خوبه؟')],
                    [telegram.KeyboardButton('نظرها به من')], [telegram.KeyboardButton('لینکمو بساز')]]
        reply_markup = telegram.ReplyKeyboardMarkup(keyboard, one_time_keyboard=True)
        bot.sendMessage(chat_id=update.message.chat_id,
                        text="*سلام\n"
                             "با این بات میتونی بگی رفیقات چنتا خوبن! یا اینکه ببینی خودت چنتا خوبی 😆*",
                        reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
        return BotState.CHOICE
    if args:
        os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'reserve_site.settings')
        django.setup()
        from rating.models import Users, Ratee
        ratee_user = Users.objects.get(share_token=args[0])
        ratee = Ratee.objects.filter(user=ratee_user)
        user_data['chat_id'] = update.message.chat_id
        if ratee:
            ratee_end = ratee[0]

        if not ratee:
            ratee_end = Ratee()
            ratee_end.ratee_user = ratee_user
            ratee_end.save()

        user_data['ratee'] = ratee_end.id
        keyboard = [[telegram.KeyboardButton('بگم چنتا🤪')], [telegram.KeyboardButton('نظر بنویسم😈')]]
        reply_markup = telegram.ReplyKeyboardMarkup(keyboard, one_time_keyboard=True)
        bot.sendMessage(chat_id=update.message.chat_id,
                        text="*می‌خوای چیکار کنی؟ 😬*",
                        reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
        return BotStateLink.MODE


def choice(bot, update, user_data):
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'reserve_site.settings')
    django.setup()
    from rating.models import Users, Rate, Ratee, Comments
    registered_db = Users.objects.filter(chat_id=user_data['chat_id'])
    if update.message.text == 'چندتا خوبم؟':
        user_data['mode'] = 1
        if registered_db:
            user_data['user'] = registered_db[0]
            ratee = Ratee.objects.filter(user__chat_id=user_data['chat_id'])
            if ratee:
                rates = Rate.objects.filter(ratee_user=ratee[0])
                if rates:
                    rate = 0
                    for user in rates:
                        rate += user.rate
                    rate = rate/rates.count()
                    bot.sendMessage(chat_id=update.message.chat_id,
                                    text=f"*در حال حاضر تو  \n"
                                         f"{rate}\n"
                                         f"تا خوبی!!!*",
                                    parse_mode=telegram.ParseMode.MARKDOWN)
                    keyboard = [[telegram.KeyboardButton('چندتا خوبم؟')], [telegram.KeyboardButton('چندتا خوبه؟')],
                                [telegram.KeyboardButton('نظرها به من')], [telegram.KeyboardButton('لینکمو بساز')]]
                    reply_markup = telegram.ReplyKeyboardMarkup(keyboard, one_time_keyboard=True)
                    bot.sendMessage(chat_id=user_data['chat_id'],
                                    text="*دیگه چیکار می‌تونم برات بکنم؟*",
                                    reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
                    return BotState.CHOICE
                else:
                    bot.sendMessage(chat_id=update.message.chat_id,
                                    text=f"*کسی هنوز نگفته چنتا خوبی لینکتو بساز و بده به دوستات تا بفهمی*",
                                    parse_mode=telegram.ParseMode.MARKDOWN)
                    keyboard = [[telegram.KeyboardButton('چندتا خوبم؟')], [telegram.KeyboardButton('چندتا خوبه؟')],
                                [telegram.KeyboardButton('نظرها به من')], [telegram.KeyboardButton('لینکمو بساز')]]
                    reply_markup = telegram.ReplyKeyboardMarkup(keyboard, one_time_keyboard=True)
                    bot.sendMessage(chat_id=user_data['chat_id'],
                                    text="*دیگه چیکار می‌تونم برات بکنم؟*",
                                    reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
                    return BotState.CHOICE

            else:
                bot.sendMessage(chat_id=update.message.chat_id,
                                text=f"*کسی هنوز نگفته چنتا خوبی لینکتو بساز و بده به دوستات تا بفهمی*",
                                parse_mode=telegram.ParseMode.MARKDOWN)
                keyboard = [[telegram.KeyboardButton('چندتا خوبم؟')], [telegram.KeyboardButton('چندتا خوبه؟')],
                            [telegram.KeyboardButton('نظرها به من')], [telegram.KeyboardButton('لینکمو بساز')]]
                reply_markup = telegram.ReplyKeyboardMarkup(keyboard, one_time_keyboard=True)
                bot.sendMessage(chat_id=user_data['chat_id'],
                                text="*دیگه چیکار می‌تونم برات بکنم؟*",
                                reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
                return BotState.CHOICE

        else:
            reply_markup = telegram.ReplyKeyboardMarkup(
                [[telegram.KeyboardButton('ثبت نام', request_contact=True)]], one_time_keyboard=True)
            bot.sendMessage(chat_id=update.message.chat_id,
                            text="*برای این که ببینی چنتا خوبی دکمه‌ی زیر رو لمس کن تا بهت بگم*",
                            reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
            return BotState.REGISTER
    if update.message.text == 'چندتا خوبه؟':
        user_data['mode'] = 2
        if registered_db:
            user_data['user'] = registered_db[0]
            bot.sendMessage(chat_id=update.message.chat_id,
                            text=f"*آی دی تلگرام(injoori@) یا شماره‌ی کسی رو که می‌خوای بگی چنتا خوبه رو بزن مثل\n"
                                 f"۰۹۱۲۹۹۹۲۲۲۲*",
                            parse_mode=telegram.ParseMode.MARKDOWN)
            return BotState.Rate_ID

        else:
            reply_markup = telegram.ReplyKeyboardMarkup(
                [[telegram.KeyboardButton('ثبت نام', request_contact=True)]], one_time_keyboard=True)
            bot.sendMessage(chat_id=update.message.chat_id,
                            text="*برای این که بتونی بگی چنتا خوبه دکمه‌ی زیر رو لمس کن*",
                            reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

            return BotState.REGISTER
    if update.message.text == 'نظرها به من':
        user_data['mode'] = 3
        if registered_db:
            user_data['user'] = registered_db[0]
            me = Ratee.objects.filter(user__chat_id=user_data['chat_id'])
            if me:
                comments = Comments.objects.filter(ratee_user=me[0])
                if comments:
                    text = ''
                    for item in comments:
                        text += item.comments + "\n" + "---------------------" + "\n"
                else:
                    text = 'هنوز نطری برات ثبت نشده لینکتو بساز و به دوستات بده تا بهت نظر بدن'
            else:
                text = 'هنوز نطری برات ثبت نشده لینکتو بساز و به دوستات بده تا بهت نظر بدن'
            bot.sendMessage(chat_id=update.message.chat_id,
                            text=text,
                            parse_mode=telegram.ParseMode.MARKDOWN)
            keyboard = [[telegram.KeyboardButton('چندتا خوبم؟')], [telegram.KeyboardButton('چندتا خوبه؟')],
                        [telegram.KeyboardButton('نظرها به من')], [telegram.KeyboardButton('لینکمو بساز')]]
            reply_markup = telegram.ReplyKeyboardMarkup(keyboard, one_time_keyboard=True)
            bot.sendMessage(chat_id=user_data['chat_id'],
                            text="*دیگه چیکار می‌تونم برات بکنم؟*",
                            reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
            return BotState.CHOICE


        else:
            reply_markup = telegram.ReplyKeyboardMarkup(
                [[telegram.KeyboardButton('شمارت', request_contact=True)]], one_time_keyboard=True)
            bot.sendMessage(chat_id=update.message.chat_id,
                            text="*برای این که بتونی نظرهایی که بهت دادن رو ببینی دکمه‌ی زیر رو لمس کن*",
                            reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

            return BotState.REGISTER
    if update.message.text == 'لینکمو بساز':
        user_data['mode'] = 4
        if registered_db:
            text = 'سلام 😊 \n' \
                   'لینک زیر رو بزن و ناشناس بگو چنتا خوبم \n' \
                   '👇👇 \n'
            link = f'https://t.me/chantakhoobe_bot?start={registered_db[0].share_token}'
            text += link
            bot.sendMessage(chat_id=update.message.chat_id,
                            text=text)
            keyboard = [[telegram.KeyboardButton('چندتا خوبم؟')], [telegram.KeyboardButton('چندتا خوبه؟')],
                        [telegram.KeyboardButton('نظرها به من')], [telegram.KeyboardButton('لینکمو بساز')]]
            reply_markup = telegram.ReplyKeyboardMarkup(keyboard, one_time_keyboard=True)
            bot.sendMessage(chat_id=user_data['chat_id'],
                            text="*دیگه چیکار می‌تونم برات بکنم؟*",
                            reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
            return BotState.CHOICE


        else:
            reply_markup = telegram.ReplyKeyboardMarkup(
                [[telegram.KeyboardButton('ثبت نام', request_contact=True)]], one_time_keyboard=True)
            bot.sendMessage(chat_id=update.message.chat_id,
                            text="*برای این که بتونی نظرهایی که بهت دادن رو ببینی دکمه‌ی زیر رو لمس کن*",
                            reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

            return BotState.REGISTER


def rate_id(bot,update, user_data):
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'reserve_site.settings')
    django.setup()
    from rating.models import Users, NotRegisteredUser
    user_name = None
    name = NotRegisteredUser.objects.all().count() + NotRegisteredUser.objects.count()
    if update.message.text[0] == '@':
        user_name = update.message.text[1:]
        user_id, status = get_user_id(name=str(name), username=update.message.text[1:])
    else:
        if update.message.text[0] == '۰':
            n = ''
            trans = {"۱": "1", "۲": "2", "۳": "3", "۴": "4", "۵": "5", "۶": "6", "۷": "7", "۸": "8", "۹": "9", "۰": "0"}
            for number in update.message.text:
                if number in trans:
                    na = trans[number]
                    n = n + na
            number = n
            number = '+98' + number
        else:
            number = '+98'+update.message.text[1:]
        ratee_user = Users.objects.filter(phone=number)
        ratee_user_not_registered = NotRegisteredUser.objects.filter(phone=number)
        if ratee_user:
            user_id = ratee_user[0].user_id
        if ratee_user_not_registered:
            user_id = ratee_user_not_registered[0].user_id
        else:
            user_id, status = get_user_id(name=str(name), phone=number)
    if user_id is not None:
        ratee_user = Users.objects.filter(user_id=user_id)
        ratee_user_not_registered = NotRegisteredUser.objects.filter(user_id=user_id)
        if ratee_user:
            if user_name is not None:
                ratee_user[0].username = user_name
                ratee_user[0].save()
            user_data['ratee'] = ratee_user[0].user_id
            user_data['registered'] = True
            try:
                if not ratee_user[0].image:
                    get_photo(user_model=ratee_user[0], user_id=user_id)
                f = open('media_cdn/' + ratee_user[0].image.url, 'rb')
                bot.send_photo(chat_id=user_data['chat_id'], photo=f)
                f.close()
            except:
                bot.sendMessage(chat_id=update.message.chat_id,
                                text="*این رفیقمون عکس نداره*", parse_mode=telegram.ParseMode.MARKDOWN)

            keyboard = [[telegram.KeyboardButton('بگم چنتا🤪')], [telegram.KeyboardButton('نظر بنویسم😈')]]
            reply_markup = telegram.ReplyKeyboardMarkup(keyboard, one_time_keyboard=True)
            bot.sendMessage(chat_id=update.message.chat_id,
                            text="*می‌خوای چیکار کنی؟ 😬*",
                            reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

        elif ratee_user_not_registered:
            user_data['ratee'] = ratee_user_not_registered[0].user_id
            user_data['registered'] = False
            try:
                if not ratee_user[0].image:
                    get_photo(user_model=ratee_user[0], user_id=user_id)
                f = open('media_cdn/' + ratee_user_not_registered[0].image.url, 'rb')
                bot.send_photo(chat_id=user_data['chat_id'], photo=f)
                f.close()
            except:
                bot.sendMessage(chat_id=update.message.chat_id,
                                text="*این رفیقمون عکس نداره*", parse_mode=telegram.ParseMode.MARKDOWN)
            keyboard = [[telegram.KeyboardButton('بگم چنتا🤪')], [telegram.KeyboardButton('نظر بنویسم😈')]]
            reply_markup = telegram.ReplyKeyboardMarkup(keyboard, one_time_keyboard=True)
            bot.sendMessage(chat_id=update.message.chat_id,
                            text="*می‌خوای چیکار کنی؟ 😬*",
                            reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

        else:
            nru = NotRegisteredUser()
            if update.message.text[0] == '@':
                nru.user_name = update.message.text[1:]
            else:
                nru.phone = number
            nru.user_id = user_id
            nru.save()
            get_photo(user_model=nru, user_id=user_id)
            user_data['ratee'] = nru.user_id
            user_data['registered'] = False
            try:
                f = open('media_cdn/' + nru.image.url, 'rb')
                bot.send_photo(chat_id=user_data['chat_id'], photo=f)
                f.close()
            except:
                bot.sendMessage(chat_id=update.message.chat_id,
                                text="*این رفیقمون عکس نداره*", parse_mode=telegram.ParseMode.MARKDOWN)
            keyboard = [[telegram.KeyboardButton('بگم چنتا🤪')], [telegram.KeyboardButton('نظر بنویسم😈')]]
            reply_markup = telegram.ReplyKeyboardMarkup(keyboard, one_time_keyboard=True)
            bot.sendMessage(chat_id=update.message.chat_id,
                            text="*می‌خوای چیکار کنی؟ 😬*",
                            reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

        return BotState.CHOOSEFUNC

    else:
        bot.sendMessage(chat_id=update.message.chat_id,
                        text="کسی رو که گفتی پیدا نکردم دوباره تلاش کن",
                        parse_mode=telegram.ParseMode.MARKDOWN)
        return BotState.Rate_ID


def choose_func(bot, update, user_data):
    if update.message.text == 'بگم چنتا🤪':
        keyboard = list()
        for i in range(1, 11):
            keyboard.append([telegram.InlineKeyboardButton(f'{i}', callback_data=f'{i}')])
        reply_markup = telegram.InlineKeyboardMarkup(keyboard)
        bot.sendMessage(chat_id=update.message.chat_id,
                        text="*بگو چنتا خوبه؟*",
                        reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
        return BotState.RATE
    elif update.message.text == 'نظر بنویسم😈':
        bot.sendMessage(chat_id=update.message.chat_id,
                        text="*نظرتو بنویس ناشناس ارسال می‌شه واسش😌*",
                        parse_mode=telegram.ParseMode.MARKDOWN)
        return BotState.COMMENT


def rate(bot, update, user_data):
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'reserve_site.settings')
    django.setup()
    from rating.models import Ratee, Rate, Users, NotRegisteredUser
    query = update.callback_query
    user_id = int(user_data['ratee'])
    ratee = Ratee.objects.filter(Q(user__user_id=user_id) | Q(notuser__user_id=user_id))
    if not ratee:
        ratee = Ratee()
        if user_data['registered'] is True:
            user = Users.objects.get(user_id=user_data['ratee'])
        else:
            user = NotRegisteredUser.objects.get(user_id=user_data['ratee'])
        ratee.ratee_user = user
        ratee.save()
    else:
        ratee = ratee[0]
    rate = Rate.objects.filter(ratee_user=ratee, rating_user=user_data['user'])
    if rate:
        bot.sendMessage(chat_id=user_data['chat_id'],
                        text="*یه بار گفتی دیگه😒*",
                        parse_mode=telegram.ParseMode.MARKDOWN)
    else:
        rate = Rate()
        rate.ratee_user = ratee
        rate.rating_user = user_data['user']
        rate.rate = int(query['data'])
        rate.save()

        a = Rate.objects.filter(ratee_user=ratee)
        total = 0
        for i in a:
            total += i.rate
        average = total/a.count()
        bot.sendMessage(chat_id=user_data['chat_id'],
                        text="*اکی شد\n"
                             f"تا الان این دوستمون \n"
                             f"{average}\n"
                             f"تا خوبه!!!*",
                        parse_mode=telegram.ParseMode.MARKDOWN)
        if user_data['registered'] is True:
            bot.sendMessage(chat_id=user_data['ratee'],
                            text=f"*یه نفر بهت گفته  \n"
                                 f"{rate.rate}\n"
                                 f"تا خوبی!!!*",
                            parse_mode=telegram.ParseMode.MARKDOWN)

    keyboard = [[telegram.KeyboardButton('چندتا خوبم؟')], [telegram.KeyboardButton('چندتا خوبه؟')],
                [telegram.KeyboardButton('نظرها به من')], [telegram.KeyboardButton('لینکمو بساز')]]
    reply_markup = telegram.ReplyKeyboardMarkup(keyboard, one_time_keyboard=True)
    bot.sendMessage(chat_id=user_data['chat_id'],
                    text="*دیگه چیکار می‌تونم برات بکنم؟*",
                    reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
    return BotState.CHOICE


def comment(bot, update, user_data):
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'reserve_site.settings')
    django.setup()
    from rating.models import Ratee, Comments, NotRegisteredUser, Users
    user_id = int(user_data['ratee'])
    ratee = Ratee.objects.filter(Q(user__user_id=user_id) | Q(notuser__user_id=user_id))
    if not ratee:
        ratee = Ratee()
        if user_data['registered'] is True:
            user = Users.objects.get(user_id=user_data['ratee'])
        else:
            user = NotRegisteredUser.objects.get(user_id=user_data['ratee'])
        ratee.ratee_user = user_data['ratee']
        ratee.save()
    else:
        ratee = ratee[0]
    comment = Comments()
    comment.ratee_user = ratee
    comment.rating_user = user_data['user']
    comment.comments = update.message.text
    comment.save()

    bot.sendMessage(chat_id=user_data['chat_id'],
                    text="نظرت ثبت شد",
                    parse_mode=telegram.ParseMode.MARKDOWN)
    if user_data['registered'] is True:
        bot.sendMessage(chat_id=user_data['ratee'],
                        text=f"*یه نفر بهت نظر جدید داده و گفته که: \n"
                             f"{comment.comments}*",
                        parse_mode=telegram.ParseMode.MARKDOWN)

    keyboard = [[telegram.KeyboardButton('چندتا خوبم؟')], [telegram.KeyboardButton('چندتا خوبه؟')],
                [telegram.KeyboardButton('نظرها به من')], [telegram.KeyboardButton('لینکمو بساز')]]
    reply_markup = telegram.ReplyKeyboardMarkup(keyboard, one_time_keyboard=True)
    bot.sendMessage(chat_id=user_data['chat_id'],
                    text="*دیگه چیکار می‌تونم برات بکنم؟*",
                    reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
    return BotState.CHOICE


def register(bot, update, user_data):
    if update.message.contact.phone_number[0] == '+':
        phone = update.message.contact.phone_number
    else:
        phone = '+' + update.message.contact.phone_number

    user_id = update.message.contact.user_id

    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'reserve_site.settings')
    django.setup()
    from rating.models import Users, NotRegisteredUser, Ratee
    u = NotRegisteredUser.objects.filter(user_id=user_id)
    if u:
        user = Users()
        user.user_id = u[0].user_id
        user.username = u[0].user_name
        user.phone = phone
        user.chat_id = update.message.chat_id
        user.save()
        ratee = Ratee.objects.filter(notuser__user_id=u[0].user_id)
        if ratee:
            ratee[0].ratee_user = user
            ratee[0].save()
        u.delete()
    else:
        user = Users()
        user.user_id = user_id
        user.phone = phone
        user.chat_id = update.message.chat_id
        user.save()

    keyboard = [[telegram.KeyboardButton('چندتا خوبم؟')], [telegram.KeyboardButton('چندتا خوبه؟')],
                [telegram.KeyboardButton('نظرها به من')], [telegram.KeyboardButton('لینکمو بساز')]]
    reply_markup = telegram.ReplyKeyboardMarkup(keyboard, one_time_keyboard=True)
    bot.sendMessage(chat_id=user_data['chat_id'],
                    text="*خب ثبت‌نام شدی چیکار می‌خوای برات بکنم؟*",
                    reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
    return BotState.CHOICE


def invalid(bot, update, user_data):
    reply_markup = telegram.ReplyKeyboardRemove()
    update.message.reply_text('دستوری که دادی نامعتبره دوباره تلاش کن', reply_markup=reply_markup)
    keyboard = [[telegram.KeyboardButton('چندتا خوبم؟')], [telegram.KeyboardButton('چندتا خوبه؟')],
                [telegram.KeyboardButton('نظرها به من')], [telegram.KeyboardButton('لینکمو بساز')]]
    reply_markup = telegram.ReplyKeyboardMarkup(keyboard, one_time_keyboard=True)
    bot.sendMessage(chat_id=user_data['chat_id'],
                    text="*دیگه چیکار می‌تونم برات بکنم؟*",
                    reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
    return BotState.CHOICE


def command_invalid(bot, update, user_data, args):
    return start(bot, update, user_data, args)











