from django.db import models


def upload_location(instance, filename):
    return "%s/%s" % (instance.name, filename)


class Comments(models.Model):

    ratee_user = models.ForeignKey(to='rating.Ratee',
                                   on_delete=models.CASCADE)

    rating_user = models.ForeignKey(to='rating.Users',
                                    on_delete=models.CASCADE)

    comments = models.TextField()

    file = models.FileField(blank=True, null=True, upload_to=upload_location)

    is_anonymous = models.BooleanField(default=False)
