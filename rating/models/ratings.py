from django.db import models


class Rate(models.Model):

    ratee_user = models.ForeignKey(to='rating.Ratee',
                                   on_delete=models.CASCADE)

    rating_user = models.ForeignKey(to='rating.Users',
                                    on_delete=models.CASCADE)

    rate = models.IntegerField()


