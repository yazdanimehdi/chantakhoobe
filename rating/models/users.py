from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.crypto import get_random_string
from django.contrib.contenttypes.fields import GenericRelation, GenericForeignKey
from django.contrib.contenttypes.models import ContentType


def upload_location(instance, filename):
    return "%s/%s" % (instance.user_id, filename)


class Ratee(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    ratee_user = GenericForeignKey()


class Users(models.Model):
    username = models.CharField(max_length=20, blank=True, null=True)
    user_id = models.IntegerField()
    chat_id = models.CharField(max_length=20)
    share_token = models.CharField(unique=True, max_length=10, default=None)
    api_auth_token = models.CharField(unique=True, max_length=200, default=None)
    phone = models.CharField(max_length=11)
    user_id = models.IntegerField()
    image = models.ImageField(upload_to=upload_location,
                              width_field='width', height_field='height', blank=True, null=True)
    height = models.IntegerField(default=0)
    width = models.IntegerField(default=0)
    ratee_user = GenericRelation(Ratee, object_id_field="object_id",
                                 related_query_name="user")

    def save(self, *args, **kwargs):
        if self.share_token is None:
            self.share_token = get_random_string(length=10)
        if self.api_auth_token is None:
            self.api_auth_token = get_random_string(length=200)
        super(Users, self).save(*args, **kwargs)


class NotRegisteredUser(models.Model):
    phone = models.CharField(max_length=11, blank=True, null=True)
    user_name = models.CharField(max_length=100, blank=True, null=True)
    user_id = models.IntegerField()
    image = models.ImageField(upload_to=upload_location,
                              width_field='width', height_field='height', blank=True, null=True)
    height = models.IntegerField(default=0)
    width = models.IntegerField(default=0)
    ratee_user = GenericRelation(Ratee, object_id_field="object_id",
                                 related_query_name="notuser")

    def __str__(self):
        return str(self.user_id)



