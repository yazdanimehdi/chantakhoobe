from django.core.management.base import BaseCommand


class Command(BaseCommand):

    def handle(self, *args, **options):
        import logging

        from rating.bot import choice, BotState, choose_func, comment, rate_id, rate, register, start, invalid, \
            rate_link, func_link, comment_link, BotStateLink, register_link, command_invalid

        from telegram.ext import Updater, CommandHandler, Filters, MessageHandler, ConversationHandler, \
            CallbackQueryHandler

        bot_token = '669848974:AAEt6oJe2yT6KGnCvgCIWcsTYgo5aJnJejw'
        updater = Updater(token=bot_token)
        dispatcher = updater.dispatcher
        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                            level=logging.INFO)

        rate_handler = ConversationHandler(
            entry_points=[CommandHandler('start', start, pass_user_data=True, pass_args=True)],
            states={BotState.CHOICE: [MessageHandler(Filters.text, callback=choice, pass_user_data=True)],
                    BotState.REGISTER: [MessageHandler(Filters.contact, callback=register, pass_user_data=True)],
                    BotState.Rate_ID: [MessageHandler(Filters.text, callback=rate_id, pass_user_data=True)],
                    BotState.CHOOSEFUNC: [MessageHandler(Filters.text, callback=choose_func, pass_user_data=True)],
                    BotState.RATE: [CallbackQueryHandler(callback=rate, pass_user_data=True)],
                    BotState.COMMENT: [MessageHandler(Filters.text, callback=comment, pass_user_data=True)],
                    BotStateLink.MODE: [MessageHandler(Filters.text,
                                                       callback=func_link,
                                                       pass_user_data=True)],
                    BotStateLink.REGISTER: [MessageHandler(Filters.contact,
                                                           callback=register_link,
                                                           pass_user_data=True)],
                    BotStateLink.RATE: [CallbackQueryHandler(callback=rate_link,
                                                             pass_user_data=True)],
                    BotStateLink.COMMENT: [MessageHandler(Filters.text,
                                                          callback=comment_link,
                                                          pass_user_data=True)]
                    },
            fallbacks=[MessageHandler(Filters.text, callback=invalid, pass_user_data=True),
                       CommandHandler('start', callback=command_invalid, pass_user_data=True,pass_args=True)]
        )
        dispatcher.add_handler(rate_handler)
        updater.start_polling()
